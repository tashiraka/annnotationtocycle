import std.stdio, std.string, std.conv, std.algorithm, std.array, std.parallelism, std.typecons;

version(unittest) {}
 else {
   void main(string[] args)
   {
     auto intervalFile = args[1];
     auto coordInfoFile = args[2];
     auto outFile = args[3];
     
     auto coords = readInterval(intervalFile);
     auto coordInfo = readCoordInfo(coordInfoFile);

     auto cycleInfo = getCycleInfo(coords, coordInfo);

     auto fout = File(outFile, "w");
     foreach(ci; cycleInfo) {
       foreach(i, flag; ci) {
         fout.write(flag);
         
         if(i < ci.length - 1) {
           fout.write("\t");
         }
       }
       fout.writeln;
     }
   }
 }


alias Point = Tuple!(double, "x", double, "y", double, "z");
alias CoordInfo = Tuple!(uint, "chrID", uint, "binID", string, "missingFlag",
                         uint, "startBin", uint, "endBin", double, "x",
                         double, "y", double, "z", double, "annotation");


// 0 -> a point not in the cycle, 1 -> a point in the cycle.
int[][] getCycleInfo(Point[][] points, CoordInfo[] coordInfo)
{
  auto app = appender!(int[][]);

  foreach(pointsByCycle; points) {
    auto entry = new int[](coordInfo.length);
    entry.fill(0);

    foreach(p; pointsByCycle) {
      foreach(i, ci; coordInfo) {
        if(ci.missingFlag != "missing"
           && p.x == ci.x && p.y == ci.y && p.z == ci.z) {
          entry[i] =  1;
        }
      }      
    }
    app.put(entry.dup);
  }  

  return app.data;
}

CoordInfo[] readCoordInfo(string filename)
{
  auto app = appender!(CoordInfo[]);
  foreach(line;  File(filename, "r").byLine) {
    auto fields = line.to!string.strip.split("\t");
    app.put(CoordInfo(fields[0].to!uint, fields[1].to!uint, fields[2].to!string,
                      fields[3].to!uint, fields[4].to!uint, fields[5].to!double,
                      fields[6].to!double, fields[7].to!double,
                      fields[8].to!double));
  }
  return app.data;
}


Point[][] readInterval(string filename)
{
  auto app = appender!(Point[][]);
  foreach(line; File(filename, "r").byLine) {
    app.put(line.to!string.strip.split("\t")[3..$]
            .map!(x => x.split(","))
            .map!(x => Point(x[0].to!double, x[1].to!double, x[2].to!double))
            .array);
  }
  return app.data;
}
