import sys

args = sys.argv
if(len(args) != 4):
	print "Usage: python %s <points file> <bedgraph file> <out file>" % args[0]
	quit()

pointsFile = args[1]
bedgraphFile = args[2]
outFile = args[3]

start = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
x = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
y = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
z = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
fin = open(pointsFile, 'r')
fin.readline()

for line in fin:
	fields = line.strip().split('\t')
	chrom = int(fields[0])
	start[chrom-1] += [int(fields[1])]
	x[chrom-1] += [fields[2]]
	y[chrom-1] += [fields[3]]
	z[chrom-1] += [fields[4]]


fout = open(outFile, 'w')
for line in open(bedgraphFile, 'r'):
	fields = line.strip().split('\t')
	tmpChrom = int(fields[0])
	tmpStart = int(fields[1])
	tmpEnd = int(fields[2])
	
	a = 0
	b = 0
	for i, noUse in enumerate(start[tmpChrom-1]):
		if(i == len(start[tmpChrom-1]) - 1):
			if(start[tmpChrom-1][i] <= tmpStart):
				a = i
				b = i
		else:
			if(start[tmpChrom-1][i] <= tmpStart and tmpStart <= start[tmpChrom-1][i+1]):
				a = i
			if(start[tmpChrom-1][i] <= tmpEnd and tmpEnd <= start[tmpChrom-1][i+1]):
				b = i

	for i in range(a, b + 1):
		fout.write(str(tmpChrom) + "\t" + str(start[tmpChrom-1][i]) + "\t" + x[tmpChrom-1][i] + "\t" + y[tmpChrom-1][i] + "\t" + z[tmpChrom-1][i] + "\n")

