import sys

args = sys.argv
if(len(args) != 4):
	print "Usage: python %s <bedgraph file> <threshold> <out file>" % args[0]
	quit()

inFile = args[1]
threshold = float(args[2])
outFile = args[3]


def convChr(s):
	if s == 'chrI':
		return 1;
	elif s == 'chrII':
		return 2;
	elif s == 'chrIII':
		return 3;
	elif s == 'chrIV':
		return 4;
	elif s == 'chrV':
		return 5;
	elif s == 'chrVI':
		return 6;
	elif s == 'chrVII':
		return 7;
	elif s == 'chrVIII':
		return 8;
	elif s == 'chrIX':
		return 9;
	elif s == 'chrX':
		return 10;
	elif s == 'chrXI':
		return 11;
	elif s == 'chrXII':
		return 12;
	elif s == 'chrXIII':
		return 13;
	elif s == 'chrXIV':
		return 14;
	elif s == 'chrXV':
		return 15;
	elif s == 'chrXVI':
		return 16;
	else:
		print 'ERROR: unexpected chr name'
		quit()


chrom = 1
start = 0
end = 0
value = 0.0
prevValue = threshold - 1.0

fout = open(outFile, 'w')

for line in open(inFile, 'r'):
	fields = line.strip().split('\t')
	tmpChrom = convChr(fields[0])
	tmpStart = int(fields[1])
	tmpEnd = int(fields[2])
	tmpValue = float(fields[3])
	
	# init each chrom
	if(tmpChrom != chrom):
		if(prevValue >= threshold and end - start >= 10):
			fout.write(str(chrom) + "\t" + str(start) + "\t" + str(end) + "\t" + str(value) + "\n")
		chrom = tmpChrom
		start = 0
		end = 0
		value = 0.0
		prevValue = threshold - 1.0

	# start	
	if(tmpChrom == chrom and prevValue < threshold and tmpValue >= threshold):
			start = tmpStart
			end = tmpEnd
			value += tmpValue
	# extension
	elif(tmpChrom == chrom and prevValue >= threshold and tmpValue >= threshold):
			end = tmpEnd
			value += tmpValue
	# end
	elif(tmpChrom == chrom and prevValue >= threshold and tmpValue < threshold and end - start >= 10):
			fout.write(str(chrom) + "\t" + str(start) + "\t" + str(end) + "\t" + str(value) + "\n")
			start = 0
			end = 0
			value = 0.0

	prevValue = tmpValue
