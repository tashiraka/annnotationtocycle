import sys

args = sys.argv
if(len(args) != 4):
	print "Usage: python %s <points file> <bedgraph file> <out file>" % args[0]
	quit()


pointsFile = args[1]
bedgraphFile = args[2]
outFile = args[3]


def convChr(s):
	if s == 'chrI':
		return 1;
	elif s == 'chrII':
		return 2;
	elif s == 'chrIII':
		return 3;
	elif s == 'chrIV':
		return 4;
	elif s == 'chrV':
		return 5;
	elif s == 'chrVI':
		return 6;
	elif s == 'chrVII':
		return 7;
	elif s == 'chrVIII':
		return 8;
	elif s == 'chrIX':
		return 9;
	elif s == 'chrX':
		return 10;
	elif s == 'chrXI':
		return 11;
	elif s == 'chrXII':
		return 12;
	elif s == 'chrXIII':
		return 13;
	elif s == 'chrXIV':
		return 14;
	elif s == 'chrXV':
		return 15;
	elif s == 'chrXVI':
		return 16;
	else:
		print 'ERROR: unexpected chr name'
		quit()


lines = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
start = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
values = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
counts = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
fin = open(pointsFile, 'r')
fin.readline()


for line in fin:
	fields = line.strip().split('\t')
	chrom = int(fields[0])
	lines[chrom-1] += [line]
	start[chrom-1] += [int(fields[1])]
	values[chrom-1] += [0.0]
	counts[chrom-1] += [0]

for line in open(bedgraphFile, 'r'):
	fields = line.strip().split('\t')
	tmpChrom = int(convChr(fields[0]))
	tmpStart = int(fields[1])
	tmpEnd = int(fields[2])
	tmpValue = float(fields[3])
	
	a = 0
	b = 0
	for i, noUse in enumerate(start[tmpChrom-1]):
		if(i == len(start[tmpChrom-1]) - 1):
			if(start[tmpChrom-1][i] <= tmpStart):
				a = i
				b = i
		else:
			if(start[tmpChrom-1][i] <= tmpStart and tmpStart <= start[tmpChrom-1][i+1]):
				a = i
			if(start[tmpChrom-1][i] <= tmpEnd and tmpEnd <= start[tmpChrom-1][i+1]):
				b = i

	for i in range(a, b + 1):
		values[tmpChrom-1][i] += tmpValue
		counts[tmpChrom-1][i] += 1

fout = open(outFile, 'w')
for i, tmp in enumerate(lines):
	for j, line in enumerate(tmp):
		if(counts[i][j] == 0):
			fout.write(line.strip() + "\t" + str(values[i][j]) + "\n")
		else:
			fout.write(line.strip() + "\t" + str(values[i][j] / counts[i][j]) + "\n")
