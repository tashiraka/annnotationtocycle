import sys

args = sys.argv
if(len(args) != 4):
	print "Usage: python %s <bedgraph file> <threshold> <out file>" % args[0]
	quit()

inFile = args[1]
threshold = float(args[2])
outFile = args[3]


fout = open(outFile, 'w')

for line in open(inFile, 'r'):
	fields = line.strip().split('\t')
	value = float(fields[3])
	
	if(value >= threshold):
		fout.write(line);
