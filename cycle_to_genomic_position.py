import sys;
import numpy as np
from scipy.stats import mannwhitneyu


args = sys.argv

coordInfoFile = args[1]
cycleFlagFile = args[2]
outFile = args[3]


genomicPosition = []
for line in open(coordInfoFile, 'r'):
    fields = line.rstrip().split('\t')
    genomicPosition.append([fields[0], fields[3], fields[4], fields[-1]])


fout = open(outFile, 'w')
for line in open(cycleFlagFile, 'r'):
    cycleFlag = [int(x) for x in line.rstrip().split('\t')]
    
    for i, anno in enumerate(genomicPosition):
        if cycleFlag[i] == 1:
            fout.write(str(genomicPosition[i]) + '\t')

    fout.write('\n')

