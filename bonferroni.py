import sys

args = sys.argv
pValFile = args[1]
sigLevel = float(args[2])
outFile = args[3]


pVal = []
for line in open(pValFile, 'r'):
    pVal += [float(line.rstrip())]


testSize = len(pVal)
fout = open(outFile, 'w')
for i, p in enumerate(pVal):
    if p * testSize <= sigLevel:
        fout.write(str(i) + '\t' + str(p) + '\n')
