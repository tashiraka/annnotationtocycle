import std.stdio, std.string, std.conv, std.algorithm, std.array, std.typecons;


alias CoordType = Tuple!(double, "x", double, "y", double, "z");

alias ChrInfoType = Tuple!(uint, "chrID", uint, "chrSize", uint, "binSize",
                           uint, "startBin", uint, "endBin");

version(unittest){}
 else {
   void main(string[] args)
   {
     auto coordFile = args[1];
     auto chrInfoFile = args[2];
     auto connectivityFile = args[3];
     auto annotationFile = args[4];
     auto outFile = args[5];

     auto coords = readMat!(double)(coordFile)
       .map!(x => CoordType(x[0], x[1], x[2])).array;
     auto chrInfo = readMat!(uint)(chrInfoFile)
       .map!(x => ChrInfoType(x[0], x[1], x[2], x[3], x[4])).array;
     auto connectivity = readConnectivity(connectivityFile);
     auto annotation = readBedgraphAndBinning(annotationFile, chrInfo,
                                              connectivity.length);
     /*
      * Fields info, tab separated
      * 1. chromosome ID
      * 2. bin ID
      * 3. missing flag, missing => "missing", present => "present"
      * 4. bin's start position (the left coordinates of a closed interval)
      * 5. bin's end position (the right coordinates of a closed interval)
      * 6. x
      * 7. y
      * 8. z
      * 9. annotation value (e.g. methylation level, RNA PolII occupancy)
      *
      * If 
      */
     auto fout = File(outFile, "w");
     auto coordsID = 0;
     
     foreach(binID, bin; connectivity) {
       if(connectivity[binID]) {
         fout.writeln(binIDtoChrID(binID, chrInfo), "\t",
                      binID, "\t",
                      "present", "\t",
                      binStartPosition(binID, chrInfo), "\t",
                      binEndPosition(binID, chrInfo), "\t",
                      coords[coordsID].x, "\t",
                      coords[coordsID].y, "\t",
                      coords[coordsID].z, "\t",
                      annotation[binID]);
         coordsID++;
       }
       else {
         fout.writeln(binIDtoChrID(binID, chrInfo), "\t",
                      binID, "\t",
                      "missing", "\t",
                      binStartPosition(binID, chrInfo), "\t",
                      binEndPosition(binID, chrInfo), "\t",
                      0, "\t",
                      0, "\t",
                      0, "\t",
                      annotation[binID]);

       }
     }
   }
 }


ulong binStartPosition(ulong binID, ChrInfoType[] chrInfo)
{
  foreach(ci; chrInfo) {
    if(ci.startBin <= binID && binID <= ci.endBin) {
      return (binID - ci.startBin) * ci.binSize + 1;
    }
  }
  throw new Exception("Error: The bin is not found.");
}


ulong binEndPosition(ulong binID, ChrInfoType[] chrInfo)
{
  foreach(ci; chrInfo) {
    if(ci.startBin <= binID && binID < ci.endBin) {
      return (binID - ci.startBin + 1) * ci.binSize;
    }
    else if(binID == ci.endBin) {
      return ci.chrSize;
    }
  }
  throw new Exception("Error: The bin is not found.");
}


ulong binIDtoChrID(ulong binID, ChrInfoType[] chrInfo)
{
  foreach(ci; chrInfo) {
    if(ci.startBin <= binID && binID <= ci.endBin) {
      return ci.chrID;
    }
  }
  throw new Exception("Error: The bin is not found.");
}


double[] readBedgraphAndBinning(string filename, ChrInfoType[] chrInfo,
                                ulong binNum)
{
  auto annotation = new double[](binNum);
  annotation.fill(0.0);
  
  foreach(line; File(filename, "r").byLine) {
    if(line != "" && line[0] != '#' && line[0..5] != "track"
       && line[0..7] != "browser" && line[0..5] != "chrmt"
       && line[0..4] != "chrM") {
      auto fields = line.to!string.strip().split("\t");

      auto binSize = 0;
      auto startBin = 0;

      foreach(ci; chrInfo) {
        if(ci.chrID == chrNameToChrID(fields[0])) {
          binSize = ci.binSize;
          startBin = ci.startBin;
          break;
        }
      }
      if(binSize == 0) {
        throw new Exception("ERROR: not found chromosome.");
      }

      //0-origin in bedgraph
      auto assignedBin = startBin + (fields[1].to!uint + 1) / binSize;
      annotation[assignedBin] += fields[3].to!double;
    }
  }
  
  return annotation;
}


uint chrNameToChrID(string name)
{
  switch(name) {
  case "chrI": return 0; break;
  case "chrII": return 1; break;
  case "chrIII": return 2; break;
  case "chrIV": return 3; break;
  case "chrV": return 4; break;
  case "chrVI": return 5; break;
  case "chrVII": return 6; break;
  case "chrVIII": return 7; break;
  case "chrIX": return 8; break;
  case "chrX": return 9; break;
  case "chrXI": return 10; break;
  case "chrXII": return 11; break;
  case "chrXIII": return 12; break;
  case "chrXIV": return 13; break;
  case "chrXV": return 14; break;
  case "chrXVI": return 15; break;
  default : throw new Exception("ERROR: invalid chromosome name.");
  }
}


bool[] readConnectivity(string filename)
{
  bool[] gapInfo;
  foreach(line; File(filename, "r").byLine)
    gapInfo
      ~= line.to!string.strip.split("\t")[1] == "connected" ? true : false;
  return gapInfo;
}


T[][] readMat(T)(string filename)
{
  auto matApp = appender!(T[][]);
  auto fin = File(filename, "r");
  foreach(line; fin.byLine) {
    if(line[0] != '#')
      matApp.put(line.to!string.strip.split("\t").map!(x => x.to!T).array);
  }
  return matApp.data;
}
