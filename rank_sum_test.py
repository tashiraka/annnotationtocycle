import sys, math
import numpy as np
from scipy.stats import mannwhitneyu


args = sys.argv

coordInfoFile = args[1]
cycleFlagFile = args[2]
outFile = args[3]


annotation = []
for line in open(coordInfoFile, 'r'):
    annotation.append(float(line.rstrip().split('\t')[-1]))


fout = open(outFile, 'w')
for line in open(cycleFlagFile, 'r'):
    cycleFlag = [int(x) for x in line.rstrip().split('\t')]
    groupInCycle = []
    groupOutCycle = []
    
    for i, anno in enumerate(annotation):
        if cycleFlag[i] == 1:
            groupInCycle += [anno]
        else:
            groupOutCycle += [anno]

    u, p_value = mannwhitneyu(groupInCycle, groupOutCycle)
    fout.write(str(p_value) + '\t' +
               str(math.fsum(groupInCycle) / len(groupInCycle)) + '\n')
