import std.stdio, std.string, std.conv, std.typecons, std.algorithm,
  std.array;


version(unittest) {}
 else {
   void main(string[] args)
   {
     auto cycleFile = args[1];
     auto transcriptFile = args[2];
     auto binInfoFile = args[3];
     auto outFile = args[4];

     auto binInfo = readBinInfo(binInfoFile);
     auto transcripts = readTranscript(transcriptFile);
     
     binningExonLength(transcripts, binInfo);

     auto cycles = readCycles(cycleFile);

     BinInfoEntry[][] tmp;
     foreach (key; binInfo.byKey)
       tmp ~= binInfo[key];
     tmp.sort!((a,b) => a[0].binID < b[0].binID);

     BinInfoEntry[] sortedBinInfo;
     foreach (t; tmp) sortedBinInfo ~= t;
     
     foreach (cycle; cycles)
       cycle.signalIntoCycle(sortedBinInfo);
   
     auto fout = File(outFile, "w");
     fout.writeln("#dim\tbirthTime\tdeathTime\tpersistenceLength\tinteriorVolume\tsignalSum\tsignalSumDivVertexNum\tsignalSumDivBirthTime\tsignalSumDivPersistenceLen\tsignalDivInteriorVolume");
     foreach (cycle; cycles) {
       fout.write(cycle.dim, "\t", cycle.birthTime, "\t", cycle.deathTime, "\t",
		  cycle.deathTime - cycle.birthTime, "\t", cycle.interiorVolume, "\t", cycle.signal, "\t",
		  cycle.signalDivVertexNum, "\t",
		  cycle.signalDivBirthTime, "\t",
		  cycle.signalDivPersistenceLen, "\t",
		  cycle.signalDivInteriorVolume);
       foreach (pid; cycle.pointIDs) fout.write("\t", pid);
       fout.writeln;
     }
   }
 }


Cycle[] readCycles(string filename)
{
  Cycle[] cycles;

  foreach (line; File(filename).byLine) {
    auto fields = line.to!string.strip.split("\t");
    auto dim = fields[0].to!ulong;
    auto birthTime = fields[1].to!double;
    auto deathTime = fields[2].to!double;
    auto interiorVolume = fields[3].to!double;
    auto binIDs = fields[4..$].map!(to!ulong).array;
    auto vertexNum = binIDs.length;

    cycles ~= new Cycle(dim, birthTime, deathTime, interiorVolume, vertexNum, binIDs);
  }

  return cycles;
}


class Cycle
{
  ulong dim, vertexNum;
  double birthTime, deathTime, interiorVolume;
  ulong[] pointIDs;
  double signal, signalDivVertexNum, signalDivBirthTime, signalDivPersistenceLen, signalDivInteriorVolume;

  this (ulong dimension, double b, double d, double v, ulong n, ulong[] ids)
  {
    dim = dimension;
    birthTime = b;
    deathTime = d;
    interiorVolume = v;
    vertexNum = n;
    pointIDs = ids;
  }

  void signalIntoCycle(BinInfoEntry[] binInfo)
  {
    // The length of PoitIDs and binIDs are same.
    // The pointIDs are sequential.
    // The binIDs loses the some IDs e.g. contigns not in chromosomes.
    // The pointIDs can be used as the indies of the array of the binIDs.
    signal = 0;
    foreach (pid; pointIDs) signal += binInfo[pid].signal;

    signalDivVertexNum = signal / pointIDs.length.to!double;
    signalDivBirthTime = signal / birthTime;
    signalDivPersistenceLen = signal / (deathTime - birthTime);
    signalDivInteriorVolume = signal / interiorVolume;
  }
}


void binningExonLength(Transcript[] transcripts,
                       BinInfoEntry[][string] binInfo)
{
  // If a transcript overlap a boundary of the bins,
  // the same exon length is put in the both two bins.
  foreach (transcript; transcripts) {
    if ((transcript.chr in binInfo) is null) continue;
    foreach (i, bi; binInfo[transcript.chr]) {
      // Overlap the intervals
      // signals [strat, end]
      // binInfo [start, end]
      auto exonLength = transcript.fpkm;
      //auto exonLength = transcript.fpkm * transcript.exonLength.to!double;
      //auto exonLength = transcript.fpkm * transcript.intronLength.to!double;
      //auto exonLength = transcript.exonLength.to!double;
      //auto exonLength = transcript.exons.length;
      if (transcript.start <= bi.end) { // start the overlap
        bi.signal += exonLength;
        
        foreach (biExtend; binInfo[transcript.chr][i..$]) {
          // calc the overlaps until sig end
          biExtend.signal += exonLength;
          if (transcript.end <= bi.end + 1) break;
        }
        break;
      }
    }
  }
}


Transcript[] readTranscript(string filename)
{
  Transcript[] transcripts;
  Transcript tmp;
  auto isFirst = true;
  
  foreach (line; File(filename).byLine) {
    auto fields = line.to!string.strip.split('\t');

    auto seqname = fields[0];
    //auto source = fields[1];
    auto feature = fields[2];
    auto start = fields[3].to!ulong;
    auto end = fields[4].to!ulong;
    auto score = fields[5].to!double;
    //auto strand = fields[6];
    //auto frame = fields[7];
    auto attributes = fields[8].strip.split(';');

    auto attrID = 0;
    auto gene_id = attributes[attrID++].strip.split(' ')[1].strip[1..($-1)];
    auto transcript_id = attributes[attrID++].strip.split(' ')[1].strip[1..($-1)];

    uint exon_number = 0;
    if (feature == "exon") {
      exon_number = attributes[attrID++].strip.split(' ')[1].strip[1..($-1)].to!uint;
    }
    else if (feature != "transcript") {
      throw new Exception("Unexpected feature");
    }

    if (attributes[attrID].strip.split(' ')[0] != "FPKM") 
      throw new Exception("Unexpected attribute");
 
    auto FPKM = attributes[attrID++].strip.split(' ')[1].strip[1..($-1)].to!double;
    //auto frac = attributes[attrID++].strip.split(' ')[1].strip[1..($-1)].to!double;
    //auto conf_lo = attributes[attrID++].strip.split(' ')[1].strip[1..($-1)].to!double;
    //auto conf_hi = attributes[attrID++].strip.split(' ')[1].strip[1..($-1)].to!double;
    auto cov = attributes[attrID++].strip.split(' ')[1].strip[1..($-1)].to!double;
    //auto full_read_support = attributes[attrID++].strip.split(' ')[1].strip[1..($-1)];

    if (feature == "transcript") {
      if (!isFirst) {
	transcripts ~= tmp;
      }
      isFirst = false;
      //tmp = new Transcript(seqname, start, end, FPKM);
      //tmp = new Transcript(seqname, start, end, score);
      tmp = new Transcript(seqname, start, end, cov);
    }
    else {
      tmp.exons ~= tuple(start, end);
    }
  }
  transcripts ~= tmp;
  return transcripts;
}


class Transcript
{
  string chr;
  ulong start, end;
  double fpkm;
  Tuple!(ulong, ulong)[] exons;

  this (string c, ulong s, ulong e, double a)
  {
    chr = c;
    start = s;
    end = e;
    fpkm = a;
  }

  ulong exonLength()
  {
    return exons.map!(x => x[1] - x[0] + 1).sum;
  }

  ulong intronLength()
  {
    auto sum = 0;
    foreach (i, exon; exons) {
      if (i > 0)
	sum += exons[i][0] - exons[i - 1][1] - 1;
    }
    return sum;
  }
}


class BinInfoEntry
{
  string chr;
  ulong binID, start, end;
  double signal = 0;

  this(ulong id, string c, ulong s, ulong e)
  {
    binID = id;
    chr = c;
    start = s;
    end = e;
  }
}


BinInfoEntry[][string] readBinInfo(string filename)
{
  BinInfoEntry[][string] entries;
  foreach (line; File(filename).byLine) {
    if (line[0] == '#') continue;

    auto fields = line.to!string.strip.split('\t');
    auto id = fields[0].to!ulong;
    auto chr = fields[1];
    auto start = fields[2].to!ulong;
    auto end = fields[3].to!ulong;
    entries[chr] ~= new BinInfoEntry(id, chr, start, end);
  }
    
  return entries;
}
