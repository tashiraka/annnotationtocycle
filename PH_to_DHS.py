import sys

args = sys.argv

DHSFile = args[1]
PHFile = args[2]
outFile = args[3]


DHS = []
for line in open(DHSFile):
    DHS.append(float(line.rstrip()))

fout = open(outFile, 'w')
for line in open(PHFile):
    fields = line.rstrip().split('\t')
    fout.write(fields[0] + '\t' + fields[1] + '\t' + fields[2] + '\t')

    # Additional
    localDHS = 0
    for i in range(3, fields.length):
        localDHS += DHS[int(fields[i])]

    fout.write(str(localDHS) + '\n')
