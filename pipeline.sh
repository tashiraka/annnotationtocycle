bedgraph_path=$1
interval_path=$2

bedgraph=`basename ${bedgraph_path}`
interval=`basename ${interval_path}`

./bedgraph_to_bin data/3Dcoord.xyz data/sacCer3.chrInfo data/bin.connectivity $bedgraph_path ./result/${bedgraph}.3D

./interval_to_bin $interval_path result/${bedgraph}.3D result/${bedgraph}.cycle_flags

python rank_sum_test.py result/${bedgraph}.3D result/${bedgraph}.cycle_flags result/${bedgraph}.p_values
